class UserController < ApplicationController

  def index
    @users= User.all
    render json: @users
  end

  def create
    @user = User.new(:first_name => user_params['first_name'],
                     :last_name => user_params['last_name'],
                     :hospital_id => user_params['hospital_id'],
                     :user_name => user_params['user_name'],
                     :password=> user_params['password'],
                     :is_active => user_params['is_active'])


    @roles = params['role_id']
   
    for i in @roles
      if Role.exists?(i)
        
          @user.roles << Role.find(i)
          @role_added = true
        else
          @role_added = false
          end
    end
 # if !User.exists?(hospital_id: user_params['hospital_id'], user_name: user_params['user_name'])
    if @user.valid?
      if(@role_added == true) 
         if @user.save
         render :json =>  { "status" => 'S1000' , "message" => "User created" }
         else
         render :json => {errors: "Registration Failed"}, status: 400
         end
      else 
      render :json => {errors: "Roles didn't exist"}
      end
   else
      render :json =>  {"status" => 'E1000' , "description" => @user.errors.messages}

    # end
   end
  end
  

 def update
    if User.exists?
        @user = User.find(user_params[:id])
        @user.update(user_params)
      if @user.save
      render :json =>  { "status" => 'S1000' , "message" => "User updated" }
    else
      render :json => {"status" => 'E1000' , "description" => @user.errors.messages}
    end
  else
    render :json => {errors: "User didn't find"}, status: 400
  end
end

def login
  
 username = user_params[:user_name]
 password = user_params[:password]



 user = User.find_by_user_name(username)
 # user.token_expired_at = Date.today + 30.days

 if(user_params['is_super_admin'] && user_params['hospital_id'] == "")

   if user && user.authenticate(password)

     user.token_expired_at = Date.today + 30.days


     render :json => {token: User.generate_auth_token(username),
     "message" => "Login Success", roles: user.roles}
   else
     render :json => { "message" => "Login Failed"}, status: 401
   end
 else
   if user && user.authenticate(password) && (user.hospital_id ==user_params['hospital_id'])
     # check hospital _id ???

     token_expired_at = Date.today + 30.days


     render :json => {token: User.generate_auth_token(username),expire_date: token_expired_at,
                      "message" => "Login Success", roles: user.roles}
   else
     render :json => { "message" => "Login Failed"}, status: 401
   end
 end


  
 end



def find_user_by_id
  if User.exists?(:id => user_params['id'])
    render :json => User.find(user_params['id'])
     else
  render :json => {errors: "User doesn't exist"}
  end
end


 
def reset_password
  username = user_params[:user_name]
  password = user_params[:password]
   user = User.find_by_user_name(username)
 if user && user.authenticate(password)

    render :json => User.find_by_user_name(user_params[:user_name]).update(:password => user_params[:new_password]), status: 200
  else
    render :json => {errors: "Reset password Failed"}, status: 401
  end
end

# def create_token
#   username = user_params[:user_name]
#   password =user_params[:password]
#    user = User.find_by_user_name(username)
#  if user && user.authenticate(password)
#       auth_token = User.generate_auth_token(username)
#      token_expired_at = Date.today + 30.days
#      render json: { auth_token: auth_token,expire_date: token_expired_at }
#    else
#        render json: { errors: [ { detail:"Error with your login or password" }]}, status: 401
#    end
#  end

 def get_roles
    render :json => Role.all
 end


def get_all_users

  render json: User.where(:hospital_id => user_params['id'] ).to_json(:include => :roles )
  end
  
def get_users_by_id
  if User.exists?(:id => user_params['id'])
     render json: User.find(user_params['id']).to_json(:include => :roles )
   else
  render :json => {errors: "User doesn't exist"}
end
end

def get_user_by_token
  @user = User.where(:auth_token => params['auth_token'])
   render :json => @user.to_json(:only => [:user_name,:first_name,:last_name,:hospital_id])
end

  def valdiate_token
   #  auth_token = params[:auth_token]
   # username = user_params[:user_name]
    if User.exists?( auth_token: params[:auth_token])
   @user = User.find_by_auth_token(params[:auth_token])
      # @user.token_expired_at = Date.today + 30.days
   if @user.token_expired_at > (Date.today - 1.days)
     render :json => {"status" => 'S1000',
         "auth_token" => params[:auth_token],
         "expire_date" => @user.token_expired_at
     }
   else
     render :json => { :error => { :message => "Token Expired"},"status" => 'E1000'}
   end
    else
      render :json => { :error => { :message => "user does not exists"},"status" => 'E1000'}
    end
    end



  private
  def user_params
    params.permit( :id,:hospital_id, :user_name,:password, :first_name, :last_name,:is_active,:new_password, :is_super_admin);
  end




end
