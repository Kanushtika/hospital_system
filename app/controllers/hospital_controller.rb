class HospitalController < ApplicationController

  def index
    @hospitals= Hospital.all
    render json: @hospitals
  end

  def create
    @hospital = Hospital.new(:hospital_id => hospital_params['hospital_id'],
                             :hospital_name => hospital_params['hospital_name'],
                             :address => hospital_params['address'],
                             :telephone_number => hospital_params['telephone_number'],
                             :email => hospital_params['email'],
                             :contact_person_name => hospital_params['contact_person_name'],
                             :contact_person_mobile => hospital_params['contact_person_mobile'],
                             :contact_person_email => hospital_params['contact_person_email'],
                             :is_active => hospital_params['is_active'])

    if @hospital.valid?
      if @hospital.save
        render :json => { "status" => 'S1000' , "message" => "hospital created" }
      else
        render :json => {errors: "Registration Failed"}, status: 400
      end
    else
      render :json =>  {"status" => 'E1000' , "description" => @hospital.errors.messages}

    end
  end

  def update
    if Hospital.exists?
        @hospital = Hospital.find(hospital_params[:id])
        @hospital.update(hospital_params)
      if @hospital.save
      render :json =>  { "status" => 'S1000' , "message" => "hospital Updated" }
    else
      render :json => {"status" => 'E1000' , "description" => @hospital.errors.messages}
    end
  else
    render :json => {errors: "Hospital didn't find"}, status: 400
  end
end

  def show
    @hospital = Hospital.where("hospital_id like?","#{hospital_params[:hospital_id]}%")
    render :json => @hospital

  end


  def find
    @hospital = Hospital.where("hospital_name like?" ,"#{hospital_params[:hospital_name]}%")
    render :json => @hospital
  end

  def find_by_id
     @hospital = Hospital.where(:hospital_id =>hospital_params['hospital_id'])
     render :json => @hospital
    end

def find_by_name
     @hospital = Hospital.where(:hospital_name =>hospital_params['hospital_name'])
     render :json => @hospital
    end

  def check_hospital
    if Hospital.where(:hospital_id => hospital_params['hospital_id']).exists?
    render :json => { hospital_status: Hospital.getHospitalStatus(hospital_params[:hospital_id]) }
  else
    render :json => {errors: "Hospital not exist"}
    end
  end

  def find_hospital_by_id
  if Hospital.exists?(:id => hospital_params['id'])
    render :json => Hospital.find(hospital_params['id'])
  end
end


  private
  def hospital_params
    params.permit( :id,:hospital_id, :hospital_name, :email, :address, :telephone_number, :contact_person_name, :contact_person_mobile, :contact_person_email,:is_active);
  end
  
end
