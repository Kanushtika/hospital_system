class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  skip_before_filter  :verify_authenticity_token
  helper_method :person_signed_in?, :current_user

  def user_signed_in?
    current_person.present?
  end

  def require_login!
    return true if authenticate_token
    render json: { errors: [ { detail: "Access denied" } ] }, status: 401
  end


  def current_user
    @_current_user ||= authenticate_token
  end


  def authenticate_token
  authenticate_with_http_token do |token, options|
  User.where(auth_token: token,token_created_at: Time.zone.now)
end
end

end
