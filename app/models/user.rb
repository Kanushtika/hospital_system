

class User < ApplicationRecord
  has_and_belongs_to_many :roles

  validates_uniqueness_of :user_name, :scope => :hospital_id

  validate :hospital_id_validation
  def hospital_id_validation
    if hospital_id.present?
  validates_length_of :hospital_id, in: 4..32
    end
    end



  validates :first_name,:last_name,:password, :user_name, presence: true,on: :create

  validates :first_name, :last_name, presence: true, on: :update

has_secure_password

def self.login(username,password)
  
    if(self.exists?(:user_name => username, :password_digest => password))
        return true
      else
        return false
      end
end

def self.generate_auth_token(username)
    token = SecureRandom.hex
    self.where(user_name: username).update(auth_token: token,token_created_at: Time.zone.now,token_expired_at: Time.zone.now+30.days)
    token
  end

  def self.invalidate_auth_token(username)
    self.where(user_name: username).update(auth_token: nil,token_created_at: nil)
  end

  validate :user_name_not_changed

  private

  def user_name_not_changed
    if user_name_changed? && self.persisted?
      errors.add(:user_name,  "Change of user_name not allowed!")
    end
  end
  

 validate :hospital_id_not_changed

  private

  def hospital_id_not_changed
    if hospital_id_changed? && self.persisted?
      errors.add(:hospital_id, "Change of hospital_id not allowed!")
    end
  end
 validate :is_active_not_changed

  private

  def is_active_not_changed
    if is_active_changed? && self.persisted?
      errors.add(:is_active, "Change of is_active not allowed!")
    end
  end

 

  end

