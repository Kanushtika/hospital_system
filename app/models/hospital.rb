class Hospital < ApplicationRecord

validates :hospital_id, presence: true, uniqueness: true
  validates_length_of :hospital_id, in: 4..32
  #  validates :email, :contact_person_email, :case_sensitive => false
   before_validation :downcase_email

private

def downcase_email
  self.email = email.downcase if email.present?
end
 
 before_validation :downcase_contact_person_email

private

def downcase_contact_person_email
  self.contact_person_email = contact_person_email.downcase if contact_person_email.present?
end

  validates :hospital_name, :address, :telephone_number, :contact_person_name,:is_active, :contact_person_mobile, presence: true, on: :create
validate :my_validation
def my_validation
   if hospital_name.blank? 
      errors.add(:hospital_name,"Blank of hospital_name not allowed")
   end

   if address.blank? 
      errors.add(:address,"Blank of address not allowed")
   end

   if telephone_number.blank? 
      errors.add(:telephone_number,"Blank of telephone_number not allowed")
   end

   if email.blank? 
      errors.add(:email,"Blank of email not allowed")
   end

   if contact_person_name.blank? 
      errors.add(:contact_person_name,"Blank of contact_person_name not allowed")
   end
   
   if contact_person_mobile.blank?
      errors.add(:contact_person_mobile,"Blank of contact_person_mobile not allowed")
   end

   if contact_person_email.blank? 
      errors.add(:contact_person_email,"Blank of contact_person_email not allowed")
   end

end


validate :email_validation
def email_validation
  if email.present?
      validates_format_of :email,         
      :with =>  /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  end
  if contact_person_email.present?
      validates_format_of :contact_person_email,           
      :with =>  /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  end
 end


def self.getHospitalStatus(hospital_id)
  self.find_by(:hospital_id => hospital_id).is_active
end


validate :hospital_id_not_changed

  private

  def hospital_id_not_changed
    if hospital_id_changed? && self.persisted?
      errors.add(:hospital_id, "Change of hospital_id not allowed!")
    end
  end

  validate :is_active_not_changed

  private

  def is_active_not_changed
    if is_active_changed? && self.persisted?
      errors.add(:is_active, "Change of is_active not allowed!")
    end
  end


end
