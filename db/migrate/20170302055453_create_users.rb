class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :hospital_id
      t.string :user_name
      t.string :password_digest
      t.boolean :is_active
      t.string :auth_token
      t.datetime :token_created_at
      t.datetime :token_expired_at
      t.timestamps
    end
  end
end
