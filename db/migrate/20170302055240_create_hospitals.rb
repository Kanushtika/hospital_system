class CreateHospitals < ActiveRecord::Migration[5.0]
  def change
    create_table :hospitals do |t|
      t.string :hospital_id
      t.string :hospital_name
      t.string :address
      t.string :telephone_number
      t.string :email
      t.string :contact_person_name
      t.string :contact_person_mobile
      t.string :contact_person_email
      t.boolean :is_active

      t.timestamps
    end
  end
end
