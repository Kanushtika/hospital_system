-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: hospital_system
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ar_internal_metadata`
--

DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_internal_metadata`
--

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;
INSERT INTO `ar_internal_metadata` VALUES ('environment','development','2017-03-03 13:04:53','2017-03-03 13:04:53');
/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hospitals`
--

DROP TABLE IF EXISTS `hospitals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospitals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_id` varchar(255) DEFAULT NULL,
  `hospital_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `telephone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `contact_person_mobile` varchar(255) DEFAULT NULL,
  `contact_person_email` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hospitals`
--

LOCK TABLES `hospitals` WRITE;
/*!40000 ALTER TABLE `hospitals` DISABLE KEYS */;
INSERT INTO `hospitals` VALUES (59,'123456','royal','colombo 5','0777890985','royal@gmail.com','kanushtika','0776543217','KKaushtika08@gmail.com',1,'2017-04-24 12:09:45','2017-05-23 05:11:57'),(60,'123tt','delmon','colombo 06','0777899990','delmon@gmail.com','kamsika','0776543217','kamsi@gmail.com',1,'2017-04-25 04:49:07','2017-04-26 08:31:36'),(61,'54678','Floss and Gloss','colombo 6','0112345678','floss@gmail.com','sayine','07756453421','sayine@gmail.com',1,'2017-04-27 05:06:58','2017-04-27 05:06:58'),(62,'6789054','santhi clinic','colombo 6','0774056788','santhi@gmail.com','kumar','0778906754','kumar@gmail.com',1,'2017-05-16 08:56:26','2017-05-16 08:56:26');
/*!40000 ALTER TABLE `hospitals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','2017-03-07 09:36:34','2017-03-07 09:36:34'),(2,'admin','2017-03-07 12:11:24','2017-03-07 12:11:24');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_roles_users_on_user_id` (`user_id`),
  KEY `index_roles_users_on_role_id` (`role_id`),
  CONSTRAINT `fk_rails_9dada905f6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fk_rails_e2a7142459` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` VALUES (88,104,1),(89,105,1),(90,106,1),(91,107,1),(92,108,1),(93,109,2),(94,110,1),(95,111,1),(96,112,1),(97,113,1),(98,114,1),(99,115,1),(100,116,1),(101,117,1),(102,118,1),(103,119,1),(104,120,2),(105,121,2),(106,122,1),(107,123,1),(108,124,1),(109,125,1),(110,126,1),(111,127,1),(112,128,1),(113,129,1),(114,130,1),(115,131,1),(116,132,1),(117,133,1),(118,134,1),(119,135,1),(120,136,2),(121,137,1),(122,138,2),(123,139,1);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20170302055240'),('20170302055453'),('20170302055618'),('20170307092119'),('20170307155618'),('20170307155619'),('20170308083356'),('20170308113527'),('20170313095240'),('20170313095515'),('20170313111801'),('20170313112720'),('20170316091601');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `hospital_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `auth_token` varchar(255) DEFAULT NULL,
  `token_created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (104,'fdsgyyuuhh','jdnfjnsjvbv','1201','nifrasismail','$2a$10$F/SHvv7tNkpAxUamxrbZWeL3PRcFFkdxE/oKgyo2MpPnhsEUBwSQS',1,'2017-04-18 06:05:27','2017-05-19 10:39:50',NULL,NULL),(105,'nifrasssss','jdnfjnsj','23434','nifrasismailssdf','$2a$10$0c79C2MhGznNs9H7Yhnm6e8033Q37YqOs3A9W.wx55Efn9quFKGzG',1,'2017-04-18 06:06:19','2017-05-19 10:21:04',NULL,NULL),(106,'geromy','nithadf','234567','kanushtika','$2a$10$dpR1nNxTfyqyMDt.B/qm8epQNINFauMAtdTQ2sfEDzkrp3a4XMD5O',1,'2017-04-18 06:08:19','2017-05-19 09:53:18',NULL,NULL),(107,'kani','mahendran','12345','kamsika','$2a$10$PgW0pzpfSkfZgNPcm0q.j.kBBBj7N1tbsfTfy3JiBn5hAAKa0AuK2',1,'2017-04-18 06:16:32','2017-05-19 09:52:46',NULL,NULL),(108,'puvi','jeyarasa','134075r','puvilathan','$2a$10$LBZ4wn4rd9CG5bcC3N4.q.Ofjw3f.aSKJMhzVLcULHBScXZ9ASike',1,'2017-04-18 06:25:24','2017-05-19 09:55:27',NULL,NULL),(109,'geromyyfff','nitha','12345','geromy','$2a$10$PMI5RL9WlhyH9K3KrVEowO.0cIWbqJRn2BX7Q.raqe3PU56Y8yVji',1,'2017-04-18 08:20:02','2017-05-18 09:47:25',NULL,NULL),(110,'tharufgf','manoj','123456','tharu','$2a$10$vaKiWiQ8fhiH/JlR71./x.3NUtW8HnatSzCxT.dZwI6QfoWs2IQ1O',1,'2017-04-18 08:20:55','2017-05-18 09:00:33',NULL,NULL),(111,'abi','bala','345678','abi','$2a$10$tWwx6mfBFaVMvgbX2MbA5uQCfj2lob/DW2wt5pzGuzOkt4.U5CZZO',1,'2017-04-18 08:26:39','2017-04-18 08:26:39',NULL,NULL),(112,'brinthinii','rajesh','1234','brintha','$2a$10$pdWUfVauoSMQajAowoJUpuqF7KxI.B.k7GkFAVLxxt4t7E8wy7biS',1,'2017-04-18 08:27:29','2017-05-21 15:56:11',NULL,NULL),(113,'ganu','mahendran','45678','ganu','$2a$10$.n2ad2GilCkWbChwd80hG.wwnwp2bcpfYYStsFySsdql.SQvNuTp.',1,'2017-04-18 08:39:25','2017-05-23 06:24:30','74bb5bdd85c28948f1b50c6c6c37872a','2017-05-23 06:24:30'),(114,'marinta','ravi','4321','marinta','$2a$10$4dW0fms3Pyz7FZGMRPthwuxxgXtQZbEjTUzU2V.soWRQ906J1ad5u',1,'2017-04-18 09:00:33','2017-04-18 09:00:33',NULL,NULL),(115,'priya','siva','12345','priya','$2a$10$qlWIBXOvbmHPe/sQH3RUMO/7v9OwaS6OALTDL2wW1reRFUP2ol.R6',1,'2017-04-18 09:07:08','2017-04-18 09:07:08',NULL,NULL),(116,'vimala','mahendran','123457890','vimala','$2a$10$DfiH3iyJQCvs8XmIwljbU.jXmLq/73dns/pwNxYax9yrUMIw0zAnK',1,'2017-04-18 10:59:39','2017-04-18 10:59:39',NULL,NULL),(117,'vimala','mahendran','123457890','vimalaranee','$2a$10$tfARCU4dUysLs5jYrCiTC.9LtlpPhbnK9HYrA1oOIbl2azqCp6pF6',1,'2017-04-18 11:00:47','2017-04-18 11:00:47',NULL,NULL),(118,'fgrg','erer','vxv','cxv','$2a$10$4s.L1/oIi6FAjrPJzS//f.BDHydY92UDKsjlEG37fx..sZB3PeIGC',1,'2017-04-18 11:04:52','2017-04-18 11:04:52',NULL,NULL),(119,'vithu','kengatharan','de345','vithu','$2a$10$NafNTBxri4Lfds4r0rDCie88CWCTeOtCLnO7yK0LLJmj3j4d/qduq',1,'2017-04-19 04:36:06','2017-04-19 04:36:06',NULL,NULL),(120,'thusintha','kumar','12345h','thusi','$2a$10$cL4Gv/9By/1TDxVaiUholehJaSZQuZl5BQfx/OFh72B/Qc3XmApeu',1,'2017-04-19 04:40:38','2017-04-19 04:40:38',NULL,NULL),(121,'abithira','jeya','nhu67','abiththira','$2a$10$g3V6.KXi7k6P4sOinZsK7.5yGBka2K56Flxk1m96140sh.Gvpbjte',1,'2017-04-19 04:47:30','2017-04-19 04:47:30',NULL,NULL),(122,'kavi','ravi','4567','kavi','$2a$10$x4J.7q.VP485pZv.dOPBWeGDMkEKdSpftRwRN1MmD4JMbRwvtUtgy',1,'2017-04-19 04:49:31','2017-04-19 04:49:31',NULL,NULL),(123,'bbbb','cccc','1235','aaaa','$2a$10$DxNHVqwFHellENR5tJ1Zb.BXYog.jaK2pD7oRUrDU6TIOmo1TN0n.',1,'2017-04-19 04:58:01','2017-04-19 04:58:01',NULL,NULL),(124,'sayine','mahen','5678','sayine','$2a$10$2k89JvvQYcKxWiIdaxpIA.Wan845.h7x0bNmk4nU4c7QteDfRVpsu',1,'2017-04-19 05:13:05','2017-04-19 05:13:05',NULL,NULL),(125,'sayine','mahen','5678y','cccc','$2a$10$n9Sw.6soU8JbLLP8mXPzbOlRg9fIZ87slEfBJE8hodHiHoeTI/grm',1,'2017-04-19 06:19:25','2017-04-19 06:19:25',NULL,NULL),(126,'puvi','jeyam','67890','puvi','$2a$10$NeuzIfVoxrjqYSqpjPwkhu8CXrTt..uVkCDg/qPi3oIPU63R7an5.',1,'2017-04-20 05:11:44','2017-05-17 08:18:59','58a1f56f7d3d772c3270341a5201aa5e','2017-05-17 08:18:59'),(127,'shiyaam','ruba','4567','siyam','$2a$10$f8UVGw9Ns528Gxu4t4/Ff.VTYpNzPRuTMTK86NrLobz8JWvk74tZW',1,'2017-04-20 06:41:18','2017-04-20 06:41:18',NULL,NULL),(128,'shiyaam','ruba','6578','shiyam','$2a$10$xZ.Q7gbB6gwCWWpq/N.vBeywkJxkr4rEbpZ3HGf2xoEFtIvOvgGOC',1,'2017-04-20 06:43:49','2017-04-20 06:43:49',NULL,NULL),(129,'ganushtika','mahendran','1234fghji','hospitaladmin','$2a$10$db1rPTizKo.Uza8OyAE8fe.7c.hEhuwllKKLWQkQXNUqiVFMcJ9ve',1,'2017-04-20 06:45:05','2017-05-18 10:45:33','e4846e3fa81f95ee03899a5540c59bb9','2017-05-18 10:45:33'),(130,'pppp','qqqq','89890','tttt','$2a$10$.EEkCfTEn0YdKAZZawUFvecu9CQTBvg.cLlwuU59nvJ5Nh.dPJ6Hi',1,'2017-04-20 11:22:57','2017-04-20 11:22:57',NULL,NULL),(131,'ganushtika','mahendran','6789','mmmm','$2a$10$ZqWfrhJaX5jUhTVug8OlLef.Z1pVXNGFUrrNy0lNVi3I5GV2l.nHW',1,'2017-04-21 10:17:48','2017-04-21 10:17:48',NULL,NULL),(132,'enpa','jana','12345tg','enpa','$2a$10$ZCVA/8h05MhdHiTXR7Kgb.2NYBMKJQ7OA7lwTswPiL80AfDPT7HTi',1,'2017-04-27 04:40:21','2017-04-27 04:40:21',NULL,NULL),(133,'pirapa','kavusi','23445','pirapa','$2a$10$wnfW1Ij8GWD7JCy0Px51FeFIbOKkE0DBOUCfLqghamfdWu/1Gtxvu',1,'2017-04-27 04:48:27','2017-04-27 04:48:27',NULL,NULL),(134,'subee','jeya','34566','subee','$2a$10$k43n7/421IoYpxirzZibb.6lNenBdXTn12G0mxBb87OWQi3gKeZXy',1,'2017-05-02 05:18:26','2017-05-02 05:18:26',NULL,NULL),(135,'ddddd','eeee','bbbbb','aaaaaaa','$2a$10$iHyIEryfc0.bwZDmAB6BaOwJBHMyINlrhgIfUA4pqVLMzBY9eWrj6',1,'2017-05-02 05:20:28','2017-05-02 05:20:28',NULL,NULL),(136,'mithusa','raam','45678','mithu','$2a$10$unuUR8Ac0KjGFHn3j/ZMo.pWczlyr.dP.r997eTTfyp1CjMK7t9vC',1,'2017-05-02 05:36:37','2017-05-02 05:36:37',NULL,NULL),(137,'thanuja','rakesh','9098765','thanuja','$2a$10$xCb8V/DDobshDRxxBFCIy.Pf2eipNnpXP.SQHRBfC.r11hblgzi3O',1,'2017-05-03 05:19:08','2017-05-03 05:19:08',NULL,NULL),(138,'araniya','bala','1234890ok','arani','$2a$10$qVgyLTXo0UTzDAZYzMmlxuq.4GOpibc9OKx19eCH0anXJw1jJbNE2',1,'2017-05-19 11:04:52','2017-05-19 11:04:52',NULL,NULL),(139,'fjnhgfj','hkjhk','fjhgmj7','gfjnhgfmj','$2a$10$u7g8WoiLsoddxSHYRf1gjeHG2gn/pU3AhpDseYVTcyJQJUpOA4btG',1,'2017-05-22 10:59:42','2017-05-22 10:59:42',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-23 13:22:33
