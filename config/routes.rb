Rails.application.routes.draw do

  get 'api/hospital' => 'hospital#index'
  post 'api/hospital' => 'hospital#create'
  put 'api/hospital/:id' => 'hospital#update'
  get 'api/hospital/like/id/:hospital_id' => 'hospital#show'
  get 'api/hospital/like/name/:hospital_name'=> 'hospital#find'
  get 'api/hospital/id/:hospital_id' => 'hospital#find_by_id'
  get 'api/hospital/name/:hospital_name' => 'hospital#find_by_name'
  get 'api/hospital/status/:hospital_id' => 'hospital#check_hospital'
  get 'api/hospital/:id' => 'hospital#find_hospital_by_id'

  get 'api/user' => 'user#index'
  post 'api/user' => 'user#create'
  put 'api/user/:id' => 'user#update'
  post 'api/user/sign_in' => 'user#login'
  post 'api/user/reset' => 'user#reset_password'
  # post 'api/user/token' => 'user#create_token'
  get 'api/user/roles' => 'user#get_roles'
get 'api/user/all/:id' => 'user#get_all_users'
get 'api/user/:id' => 'user#find_user_by_id'
get 'api/user/id/:id' => 'user#get_users_by_id'
get 'api/user/token/:auth_token' => 'user#get_user_by_token'
post 'api/user/validate_token' => 'user#valdiate_token'
delete 'api/user/logout' => 'user#destroy'
  

end
