import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  template: '<alert></alert><router-outlet></router-outlet>',
})
export class SimpleLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }
}