import { Injectable }    from '@angular/core';
import {  Http, Headers } from '@angular/http';
import {Hospital} from './../models/hospital'
import {Observable} from 'rxjs';

@Injectable()
export class HospitalService {

  constructor(private http: Http) { }

  getAHospitalDetails(id: string) {
    var URL = 'http://localhost:3000/api/hospital/' + id;
    return this.http.get(URL).map(res => res.json());
}

createNewHospital(hospital_model: Hospital) {
    let params = JSON.stringify({
      hospital_name:hospital_model.hospital_name,
      address: hospital_model.address,
      hospital_id:hospital_model.hospital_id,
      telephone_number: hospital_model.telephone_number,
      email: hospital_model.email,
      contact_person_name: hospital_model.contact_person_name,
      contact_person_mobile: hospital_model.contact_person_mobile,
      contact_person_email: hospital_model.contact_person_email,
      is_active:hospital_model.is_active
      
    });
    console.log("From Service " + params);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/hospital/' , params, {headers: headers})
      .map(res => res.json());
  }



updateAHospital(hospital_model: Hospital) {
    let params = JSON.stringify({
      hospital_name:hospital_model.hospital_name,
      hospital_id:hospital_model.hospital_id,
      address: hospital_model.address,
      telephone_number: hospital_model.telephone_number,
      email: hospital_model.email,
      contact_person_name: hospital_model.contact_person_name,
      contact_person_mobile: hospital_model.contact_person_mobile,
      contact_person_email: hospital_model.contact_person_email,
      is_active:hospital_model.is_active
      
    });
    console.log("From Service " + params);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/api/hospital/' +hospital_model.id , params, {headers: headers})
      .map(res => res.json());
  }


getAllHospitals() {
    var URL = 'http://localhost:3000/api/hospital';
    return this.http.get(URL).map(res => res.json());
}

    getAHospitalByHospitalId(hospital_id: string) {
        var URL = 'http://localhost:3000/api/hospital/id/' + hospital_id;
        return this.http.get(URL).map(res => res.json());
    }


}