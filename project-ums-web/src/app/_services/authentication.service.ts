import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {User} from './../models/user'

@Injectable()
export class AuthenticationService {
    constructor(private http: Http) { }



    login(is_super_admin:boolean,hospital_id: string, user_name: string, password: string) {

        if(is_super_admin){
            hospital_id = ""
        }
        let headers = new Headers();
        console.log("From Service" + is_super_admin);
    headers.append('Content-Type', 'application/json');
        var url = 'http://localhost:3000/api/user/sign_in';
        return this.http.post(url, JSON.stringify({ is_super_admin: is_super_admin,hospital_id: hospital_id,user_name: user_name, password: password }), {headers: headers})
            .map(res => res.json());

    }




    logout() {
        // remove user from local storage to log user out

        localStorage.removeItem('currentUser');
        // localStorage.removeItem('token_expired_at');
    }

    validateToken(auth_token:string) {
        let params = JSON.stringify({
            auth_token: auth_token

        });
        console.log(" From Service " + params);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:3000/api/user/validate_token' , params, {headers: headers})
            .map(res => res.json());

    }


}