import { Injectable } from '@angular/core';
import {  Http, Headers, RequestOptions, } from '@angular/http';
import {User} from './../models/user'
import "rxjs/add/operator/map"
import {Role} from  './../models/role'
@Injectable()
export class UserService {

  constructor(private http: Http ) { }


getAUserDetails(id: string) {
    var URL = 'http://localhost:3000/api/user/' + id;
    return this.http.get(URL).map(res => res.json());

}

getAUserDetailsByToken(auth_token: string) {
    var URL = 'http://localhost:3000/api/user/token' + auth_token;
    return this.http.get(URL).map(res => res.json());

}

getAllUserDetails(id: string) {
    var URL = 'http://localhost:3000/api/user/id/'+id;
    return this.http.get(URL).map(res => res.json());
}

createNewUser(user_model: User) {
    let params = JSON.stringify({
      first_name:user_model.first_name,
      last_name: user_model.last_name,
      hospital_id:user_model.hospital_id,
      user_name: user_model.user_name,
      password: user_model.password,
      is_active:user_model.is_active,
      role_id:user_model.role_id
    });
    console.log(" From Service " + params);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/user/' , params, {headers: headers})
      .map(res => res.json());

  }


  getRoles() {
    var URL = 'http://localhost:3000/api/user/roles';
    return this.http.get(URL).map(res => res.json());
}

updateAUser(user_model: User) {
    let params = JSON.stringify({
      first_name:user_model.first_name,
      last_name: user_model.last_name
    });
  
    console.log(" From Service " + params);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/api/user/'+user_model.id, params, {headers: headers})
      .map(res => res.json());

  }

  userLogin(user_model: User) {
    let params = JSON.stringify({
      
      user_name: user_model.user_name,
      password: user_model.password,
      
    });
    console.log(" From Service " + params);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/user/sign_in' , params, {headers: headers})
      .map(res => res.json());

  }

   hospitalAdminLogin(user_model: User) {
    let params = JSON.stringify({
      hospital_id:user_model.hospital_id,
      user_name: user_model.user_name,
      password: user_model.password,
      
    });
    console.log(" From Service " + params);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/user/sign_in' , params, {headers: headers})
      .map(res => res.json());

  }

getHospitalID() {
    var URL = 'http://localhost:3000/api/user';
    return this.http.get(URL).map(res => res.json());
}

getAllUsers(id:string) {
    var URL = 'http://localhost:3000/api/user/all/' + id ;
    return this.http.get(URL).map(res => res.json());
}


    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}