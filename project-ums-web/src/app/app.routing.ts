import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
//Layouts
import {FullLayoutComponent} from "./layouts/full-layout.component";
import {FullLayout2Component} from "./layouts/full-layout2.component";
import {SimpleLayoutComponent} from "./layouts/simple-layout.component";
import {AuthGuard} from "./_guards/auth.guard";
export const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard]
            },
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule', canActivate: [AuthGuard]
            },

            {
                path: 'hospitals',
                loadChildren: './hospitals/hospitals.module#HospitalsModule', canActivate: [AuthGuard]
            },
        ]
    },

    {
        path: 'pages',
        component: SimpleLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: '',
                loadChildren: './pages/pages.module#PagesModule'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'dashboard2',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayout2Component,
        data: {
            title: 'Home2'
        },
        children: [
            {
                path: 'dashboard2',
                loadChildren: './dashboard2/dashboard2.module#Dashboard2Module', canActivate: [AuthGuard]
            },

        ]
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
