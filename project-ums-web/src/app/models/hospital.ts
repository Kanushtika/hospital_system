export class Hospital {
  constructor(
     public id:string,
    public hospital_name: string,
    public hospital_id: string,
    public address: string,
    public telephone_number: string,
    public email: string,
    public contact_person_name: string,
    public contact_person_mobile:string,
    public contact_person_email: string,
    public is_active: boolean){}
}
