import {Role} from './role';
export class User{
  constructor(
    public user_name: string,
    public id:string,
    public first_name:string,
    public last_name:string,
    public hospital_id:string,
    public is_active:boolean,
    public role_id:any[],
    public auth_token?:string,
    public password?:string,
    public confirm_password?: string,
    public role?:Role,
    ){}
}