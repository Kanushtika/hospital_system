import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './../_services/authentication.service';
import {User} from './../models/user'
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()

export class AuthGuard implements CanActivate {
    status: string;
    constructor(private router: Router,private authenticationService: AuthenticationService,) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            //validate the token value
this.authenticationService.validateToken(localStorage.getItem('currentUser')).subscribe(
    data => {
        console.log(data),
            this.status = JSON.stringify(data["status"])
console.log(status)
console.log(localStorage.getItem('currentUser'))

    },
    error => alert(error),
    () => {
        if (this.status.includes("S1000")) {
            console.log(true)
return true;

        }
        else {
            console.log(false)
            this.router.navigate(['/pages/login'])
            return false;

        }
    }

);
                       return true;



        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/pages/login']);
        return false;
    }
}