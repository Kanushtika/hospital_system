import { NgModule } from '@angular/core';


import { UserLoginComponent } from './user-login.component';
import { HttpModule } from '@angular/http';

import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuard } from './../_guards/auth.guard';

import { PagesRoutingModule } from './pages-routing.module';
import { DashboardComponent } from './../dashboard/dashboard.component';


//import {AlertComponent} from './../_directives/alert.component'
@NgModule({
  imports: [ PagesRoutingModule,HttpModule,
   CommonModule,
   FormsModule ],
  declarations: [
    UserLoginComponent
  
  ],
  // providers: [AuthGuard]
})
export class PagesModule { }