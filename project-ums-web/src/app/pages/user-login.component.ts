import { Component, OnInit } from '@angular/core';
import { User } from './../models/user';
import { UserService } from './../_services/user.service'
import { AuthenticationService } from './../_services/authentication.service'
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from './../_services/alert.service'
import {AuthGuard} from './../_guards/auth.guard';

@Component({
   
    templateUrl: './user-login.component.html',

    providers: [UserService,AuthenticationService]
})
export class UserLoginComponent implements OnInit {
    user_model = new User('', '', '', '', 'Select Your Hospital ID', false, []);
    loading = false;
    returnUrl: string;
     hospital_details = [];
     is_super_admin = false;
    status: string;
    result: any;
    role_models = [];

    constructor(private _httpService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) {


    }
    ngOnInit() {
  this._httpService.getHospitalID().subscribe(
        data => this.hospital_details = data,
        error => alert(error),
        () => { console.log(this.user_model)}
     );
        this._httpService.getRoles().subscribe(
            data => this.role_models = data,
            error => alert(error),
            () => { console.log(this.role_models) }
        );
        // this.authenticationService.validateToken(this.user_model).subscribe(
        //     data => {
        //         console.log(data),
        //             this.status = JSON.stringify(data["status"]),
        //             this.result = data
        //     },
        //     error => alert(error),
        //     () => {
        //         if (this.status.includes("S1000")) {
        //
        //             return true;
        //
        //         }
        //         else {
        //
        //             this.router.navigate(['/pages/login'])
        //
        //
        //         }
        //     }
        //
        // );

        this.logout();

    }

    private logout() {
        this.authenticationService.logout();
        this.is_super_admin = false;
        this.user_model = new User('', '', '', '', 'Select Your Hospital ID', false, []);
        this.hospital_details = [];
    }

    login() {
        this.loading = true;

                this.authenticationService.login(this.is_super_admin,this.user_model.hospital_id, this.user_model.user_name, this.user_model.password)
                    .subscribe(
                        data => {

                            // login successful if there's a jwt token in the response



                            if (data && data.token) {
                                // store user details and jwt token in local storage to keep user logged in between page refreshes
                                localStorage.setItem('currentUser',data['token']);

                                for(let i=0; i<data['roles'].length; i++) {
                                    if ((data['roles'][i].id == 1) && (this.is_super_admin == false)) {


                                        this.alertService.error('Access Denied')
                                    }


                                    else if ((data['roles'][i].id == 2) && (this.is_super_admin == true)) {


                                        this.alertService.error('Access Denied')

                                    }
                                }
                            }

                            for(let i=0; i<data['roles'].length; i++) {
                                console.log(data['roles'][i].id)

                                if ((data['roles'][i].id == 1) && (this.is_super_admin== true)) {

                                    console.log("Login Suceess1"),
                                        this.router.navigate(['/dashboard']),
                                        this.alertService.success('Login successful', true)
                                    console.log("Passed")
                                }


                                else if ((data['roles'][i].id == 2) &&  (this.is_super_admin == false) ){

                                    console.log("Login Suceess2"),
                                        this.router.navigate(['/dashboard2']),
                                        this.alertService.success('Login successful', true)
                                    console.log("Passed")
                                }
                            }

                        },
                        error => {
                            this.alertService.error(" Login Failed ");
                            this.loading = false;
                        }

                    );


    }
    
    isSuperadmin(is_super_admin: boolean){
this.is_super_admin = is_super_admin;
console.log(this.is_super_admin)
    }

    }

