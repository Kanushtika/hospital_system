import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { UserLoginComponent } from './user-login.component';
import { AuthGuard } from './../_guards/auth.guard';
import {DashboardComponent} from './../dashboard/dashboard.component'

const routes: Routes = [

  {
    path: '',
    data: {
      title: 'Example Pages'
    },
    children: [
     
      {
        path: 'login',
        component: UserLoginComponent,
        data: {
          title: 'Login Page'
        }
      },
      
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}