import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { Dashboard2Component } from './dashboard2.component';
import { Dashboard2RoutingModule } from './dashboard2-routing.module';
import { ProfileTooltipComponent } from "app/profile-tooltip/profile-tooltip.component";

@NgModule({
    imports: [
        Dashboard2RoutingModule,
        ChartsModule
    ],
    declarations: [ Dashboard2Component ]
})
export class Dashboard2Module { }
