import { NgModule } from '@angular/core';
import { Routes,
    RouterModule } from '@angular/router';

import { Dashboard2Component } from './dashboard2.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Dashboard2'
        },
        children: [

            {
                path: 'dashboard2',
                component: Dashboard2Component,
                data: {
                    title: 'Dashboard'
                }
            },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Dashboard2RoutingModule {}
