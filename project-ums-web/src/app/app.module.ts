import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {Http, Headers, Response, HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { AuthGuard } from './_guards/auth.guard';
// Routing Module
import { AppRoutingModule } from './app.routing';
import { NglModule } from 'ng-lightning/ng-lightning';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { FullLayout2Component } from './layouts/full-layout2.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import {AlertService} from './_services/alert.service';

import { UsersModule } from './users/users.module';
import { AlertComponent } from "app/_directives/alert.component";

import { UserService } from "app/_services/user.service";
import { ProfileTooltipComponent } from "app/profile-tooltip/profile-tooltip.component";
import {AuthenticationService} from "./_services/authentication.service";



@NgModule({
  imports: [
    BrowserModule,
      HttpModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    NglModule.forRoot(),
   
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    FullLayout2Component,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
     SimpleLayoutComponent,
     AlertComponent
  
   
     
    
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },AuthGuard,AlertService,AuthenticationService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
