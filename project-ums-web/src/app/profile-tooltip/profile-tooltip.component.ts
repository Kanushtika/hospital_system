import { Component, OnInit } from '@angular/core';
import {User} from './../models/user';
import { UserService } from './../_services/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService } from './../_services/alert.service';
@Component({
  selector: 'profile-tooltip',
  templateUrl: './profile-tooltip.component.html',
  providers: [UserService]
})
export class ProfileTooltipComponent implements OnInit {
        currentUser: any;
 user_model = new User('','','','','', false,[]);
 private sub: any;
 username :string;

 constructor(private _httpService: UserService,
  private router: Router,private route: ActivatedRoute,  private alertService: AlertService
) {

   }


  ngOnInit() {
 this.sub = this.route.params.subscribe(params => {
      let auth_token= params['auth_token'];
    this._httpService.getAUserDetailsByToken(auth_token).subscribe(
        data => this.username = data['user_name'],
        error => alert(error),
        () => { console.log("")}
     );
  }
 );
}
}
