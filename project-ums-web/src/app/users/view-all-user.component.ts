import { Component, OnInit } from '@angular/core';
import { User } from './../models/user';
import { UserService } from './../_services/user.service';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Role } from './../models/role';
import { Hospital } from "app/models/hospital";
import { HospitalService } from '../_services/hospital.service';

@Component({

  templateUrl: './view-all-user.component.html',
  
  providers: [UserService,HospitalService]
})
export class ViewAllUserComponent implements OnInit {
  users: User[];
    hospital_model = new Hospital('', '', '', '', '', '', '', '', '', false)
  role_id = [];
  role_models = [];
  msgs: Message[] = [];
  roles = [];
  sub:any;
  hospital_id:string;
  hospital_name: string;
  hospital = "test"

  constructor(private _httpService: UserService,private _hospitalService: HospitalService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this.hospital_id =id;
    this._httpService.getAllUsers(id).subscribe(
      data => { this.users = data },
      error => alert(error),
      () => { this.roles = this.users['roles']; }
    );
});

      this.sub = this.route.params.subscribe(params => {
                        this._hospitalService. getAHospitalByHospitalId(this.hospital_id).subscribe(
                  data => this.hospital = data[0]['hospital_name'],
                  error => alert(error),
                  () => { console.log("") }
              );
          }
      );

  }

  onClick() {
    this.router.navigateByUrl('/users/create-user/' +this.hospital_id)
  }



 
}