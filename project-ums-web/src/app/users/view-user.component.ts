import { Component, OnInit } from '@angular/core';
import { UserService } from './../_services/user.service';
import { User } from './../models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { Role } from './../models/role';

@Component({

  templateUrl: './view-user.component.html',

  providers: [UserService]
})
export class ViewUserComponent implements OnInit {

  user_model = new User('', '', '', '', '', false, [])
  roles = [];
  private sub: any;
  constructor(private _httpService: UserService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this._httpService.getAllUserDetails(id).subscribe(
        data => this.user_model = data,

        error => alert(error),
        () => { this.roles = this.user_model['roles']; }
      );
    }
    );

  }



}