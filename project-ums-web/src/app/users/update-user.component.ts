import { Component, OnInit } from '@angular/core';
import { User } from './../models/user';
import { UserService } from './../_services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from './../_services/alert.service';
@Component({

  templateUrl: './update-user.component.html',
  providers: [UserService]
})
export class UpdateUserComponent implements OnInit {
  user_model = new User('', '', '', '', '', false, []);
  private sub: any;
  status: string;
  result: any;
  description: any;
hospital_id: string;
  error_first_name = "";
  error_last_name = "";

  constructor(private _httpService: UserService,
    private router: Router, private route: ActivatedRoute, private alertService: AlertService
  ) {

  }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
       this.hospital_id =id;
      this.user_model.hospital_id=id;
      this._httpService.getAUserDetails(id).subscribe(
        data => this.user_model = data,
        error => alert(error),
        () => { console.log("") }
      );
    }
    );
  }
  updateUser() {
    this._httpService.updateAUser(this.user_model).subscribe(
      data => {
        console.log(data),
        this.status = JSON.stringify(data["status"]),
        this.result = data
      },
      error => alert(error),
      () => {
        if (this.status.includes("S1000")) {

          this.router.navigate(['/users/view-all-user/'+this.user_model.hospital_id]),
            this.alertService.success('Update successful', true)

        }
        else {
          this.alertService.error("Oops! Failed to update user, Please correct the entered fields")
          this.description = this.result["description"]

          this.error_first_name = this.result["description"]['first_name'],
            this.error_last_name = this.result["description"]['last_name']


        }
      }

    );
  }


  cancel() {
    this.router.navigateByUrl('/users/view-all-user/'+this.user_model.hospital_id)
  }


}
