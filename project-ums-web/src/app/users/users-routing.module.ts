import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateUserComponent } from './create-user.component';
import { UpdateUserComponent } from './update-user.component';
import { ViewAllUserComponent } from './view-all-user.component';
import { ViewUserComponent } from './view-user.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users'
    },
    children: [
      {
        path: 'create-user/:id',
        component: CreateUserComponent,
        data: {
          title: 'Create User'
        }
      },

      {
        path: 'update-user/:id',
        component: UpdateUserComponent,
        data: {
          title: 'Update User'
        }
      },
      {
        path: 'view-all-user/:id',
        component: ViewAllUserComponent,
        data: {
          title: 'View All User'
        }
      },
      {
        path: 'view-user/:id',
        component: ViewUserComponent,
        data: {
          title: 'View  User'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }