import { Component, OnInit } from '@angular/core';
import { User } from './../models/user';
import { UserService } from './../_services/user.service';
import { Observable } from 'rxjs/Rx';
import { Role } from './../models/role';
import { AlertService } from './../_services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({

  templateUrl: './create-user.component.html',
  providers: [UserService]

})
export class CreateUserComponent implements OnInit {

  user_model = new User('', '', '', '', '', true, []);
  role_models = [];
  selected_role_ids = [];
  role_id = [];
  loading = false;
  status: string;
  result: any;
  description: any;
  error_user_name = "";
  error_first_name = "";
  error_last_name = "";
  error_hospital_id = "";
  error_password = "";
sub: any;
hospital_id:string;
  constructor(private _httpService: UserService,
    private alertService: AlertService, private router: Router, private route: ActivatedRoute) {


  }


  test(role: Role) {
    console.log(this.role_models[this.role_models.indexOf(role)].isChecked = !role.isChecked)
  }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this.hospital_id =id;
      this.user_model.hospital_id=id;
    });
    this._httpService.getRoles().subscribe(
      data => this.role_models = data,
      error => alert(error),
      () => { console.log(this.role_models) }
    );


  }
  register() {

    this.loading = true;
    this.selected_role_ids = [];
    for (let i = 0; i < this.role_models.length; i++) {
      if (this.role_models[i].isChecked) {
        this.selected_role_ids.push(this.role_models[i].id)
      }
    }
    this.user_model.role_id = this.selected_role_ids;
    console.log(this.user_model)
    if (this.user_model.password == this.user_model.confirm_password) {
      console.log('true');
      this._httpService.createNewUser(this.user_model).subscribe(
        data => {
          console.log(data),
          this.status = JSON.stringify(data["status"]),
          this.result = data
        },
        error => alert(error),
        () => {
          if (this.status.includes("S1000")) {

            this.router.navigate(['/users/view-all-user/'+this.hospital_id]),
              this.alertService.success('Registration successful', true)

          }
          else {
            this.alertService.error("Oops! Failed to create user, Please correct the entered fields")
            this.description = this.result["description"]
            this.error_user_name = this.result["description"]['user_name'],
              this.error_hospital_id = this.result["description"]['hospital_id'],
              this.error_first_name = this.result["description"]['first_name'],
              this.error_last_name = this.result["description"]['last_name'],
              this.error_password = this.result["description"]['password']


          }
        }

      );
    }
  }



  cancel() {
    this.router.navigateByUrl('/users/view-all-user/' +this.hospital_id)
  }


}
