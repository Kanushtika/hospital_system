import { NgModule } from '@angular/core';

import { CreateUserComponent } from './create-user.component';
import { UpdateUserComponent } from './update-user.component';
import { ViewAllUserComponent } from './view-all-user.component';
import { ViewUserComponent } from './view-user.component';
//import { AlertComponent } from './../_directives/alert.component';

import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { DataTableModule, SharedModule, MessagesModule, GrowlModule } from 'primeng/primeng';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertService } from './../_services/alert.service';

// Components Routing
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  imports: [
    UsersRoutingModule,
    HttpModule,
    CommonModule,
    FormsModule,
    DataTableModule,
    MessagesModule,
    GrowlModule,
    ModalModule.forRoot(),

  ],
  declarations: [
    CreateUserComponent,
    UpdateUserComponent,
    ViewAllUserComponent,
    ViewUserComponent


  ],

})
export class UsersModule { }