import { Component, OnInit } from '@angular/core';
import { Hospital } from './../models/hospital';
import { HospitalService } from '../_services/hospital.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({

  templateUrl: './view-hospital.component.html',

  providers: [HospitalService]
})
export class ViewHospitalComponent implements OnInit {

  hospital_model = new Hospital('', '', '', '', '', '', '', '', '', false)
  private sub: any;

  constructor(private _httpService: HospitalService,
    private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this._httpService.getAHospitalDetails(id).subscribe(
        data => this.hospital_model = data,
        error => alert(error),
        () => { console.log("") }
      );
    }
    );
  }
}
