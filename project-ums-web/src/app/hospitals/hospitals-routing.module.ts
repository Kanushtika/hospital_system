import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateHospitalComponent } from './create-hospital.component';
import { UpdateHospitalComponent } from './update-hospital.component';
import { ViewAllHospitalComponent } from './view-all-hospital.component';
import { ViewHospitalComponent } from './view-hospital.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Hospitals'
    },
    children: [
      {
        path: 'create-hospital',
        component: CreateHospitalComponent,
        data: {
          title: 'Create Hospital'
        }
      },
      {
        path: 'update-hospital/:id',
        component: UpdateHospitalComponent,
        data: {
          title: 'Update Hospital'
        }
      },
      {
        path: 'view-all-hospital',
        component: ViewAllHospitalComponent,
        data: {
          title: 'View All Hospital'
        }
      },
      {
        path: 'view-hospital/:id',
        component: ViewHospitalComponent,
        data: {
          title: 'View Hospital'
        }
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalsRoutingModule { }