import { NgModule } from '@angular/core';

import { CreateHospitalComponent } from './create-hospital.component';
import { UpdateHospitalComponent } from './update-hospital.component';
import { HttpModule } from '@angular/http';

import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ViewAllHospitalComponent } from './view-all-hospital.component';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { ViewHospitalComponent } from './view-hospital.component';

// Components Routing
import { HospitalsRoutingModule } from './hospitals-routing.module';

@NgModule({
  imports: [
    HospitalsRoutingModule,
    HttpModule,
    CommonModule,
    FormsModule,
    DataTableModule

  ],
  declarations: [
    CreateHospitalComponent,
    UpdateHospitalComponent,
    ViewAllHospitalComponent,
    ViewHospitalComponent

  ]
})
export class HospitalsModule { }