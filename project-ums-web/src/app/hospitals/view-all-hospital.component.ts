import { Component, OnInit } from '@angular/core';
import { Hospital } from './../models/hospital';
import { HospitalService } from './../_services/hospital.service';
import { Router } from '@angular/router';
@Component({

  templateUrl: './view-all-hospital.component.html',

  providers: [HospitalService]
})
export class ViewAllHospitalComponent implements OnInit {
  hospitals: Hospital[];


  constructor(private _httpService: HospitalService,
    private router: Router) { }

  ngOnInit() {

    this._httpService.getAllHospitals().subscribe(
      data => this.hospitals = data,
      error => alert(error),
      () => { console.log(this.hospitals) }
    );

  }

  create() {
    this.router.navigateByUrl('/hospitals/create-hospital')
  }

}