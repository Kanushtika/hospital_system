import { Component, OnInit } from '@angular/core';
import { Hospital } from './../models/hospital';
import { HospitalService } from '../_services/hospital.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from "app/_services/alert.service";
@Component({

  templateUrl: './update-hospital.component.html',

  providers: [HospitalService]
})
export class UpdateHospitalComponent implements OnInit {

  hospital_model = new Hospital('', '', '', '', '', '', '', '', '', false)
  private sub: any;
  status: string;
  result: any;
  description: any;
  error_hospital_name = "";
  error_address = "";
  error_email = "";
  error_hospital_id = "";

  error_telephone_number = "";
  error_contact_person_name = "";
  error_contact_person_mobile = "";
  error_contact_person_email = "";

  constructor(private _httpService: HospitalService,
    private router: Router, private route: ActivatedRoute, private alertService: AlertService) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this._httpService.getAHospitalDetails(id).subscribe(
        data => this.hospital_model = data,
        error => alert(error),
        () => { console.log("") }
      );
    }
    );
  }

  updateHospital() {
    this._httpService.updateAHospital(this.hospital_model).subscribe(
      data => {
        console.log(data),
          this.status = JSON.stringify(data["status"]),
          this.result = data
      },
      error => alert(error),
      () => {
        if (this.status.includes("S1000")) {

          this.router.navigate(['/hospitals/view-all-hospital']),
            this.alertService.success('Update successful', true)

        }
        else {
          this.alertService.error("Oops! Failed to update hospital, Please correct the entered fields")
          this.description = this.result["description"]
          this.error_hospital_name = this.result["description"]['hospital_name'],
            this.error_hospital_id = this.result["description"]['hospital_id'],
            this.error_address = this.result["description"]['address'],
            this.error_telephone_number = this.result["description"]['telephone_number'],
            this.error_email = this.result["description"]['email'],
            this.error_contact_person_name = this.result["description"]['contact_person_name'],
            this.error_contact_person_mobile = this.result["description"]['contact_person_mobile '],
            this.error_contact_person_email = this.result["description"]['contact_person_email']

        }
      }

    );
  }

  cancel() {
    this.router.navigateByUrl('/hospitals/view-all-hospital')
  }

}
